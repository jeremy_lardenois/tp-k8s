---
author: Jérémy LARDENOIS
---

# k8s

## Docker-hub

Pour mettre une image sur le docker hub, il nous faut tout d'abord y créer un repository. 

On se rend donc sur le site <https://hub.docker.com>, on s'inscrit puis on se connecte on va dans repositories, puis on clique sur Create Repository, on y met un nom et une description et on clique sur Create.

![Create repository dockerhub](images/create-repo-dockerhub.jpg)

Ensuite, on va générer un access token pour pouvoir y accéder facilement sans taper le mot de passe du compte. Pour faire cela, nous cliquons sur notre nom d'utilisateur en haut a gauche, puis dans account settings. Ensuite nous allons dans security et on clique sur New Access Token. On y rentre une description et on clique sur Generate. on pourra utiliser ce token à la place du password. On met le token de coté car on l'utilsera plusieurs fois plus tard

![My access token](images/my-access-token.jpg)

## Conteneur

### Ressources

image : <https://www.francetvinfo.fr/pictures/4gvXEiW0azyAnchhSQwp9yP13mc/752x423/2021/04/27/6087e5628c1ba_5f2adf6657660_130509pvl3200_abd0b2f2546931fc1106693381b2354a.gif>

### Creation du conteneur

On crée tout d'abord un repertoire et on s'y place dedans :

```bash
mkdir express
cd express
```

Ensuite, on crée la page html que l'on va mettre dans le conteneur:

```bash
cat index.html
```

```html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>TP-k8s</title>
</head>
<body>
    <h1>C'était le 26 mai 93</h1>
    <img src="https://france3-regions.francetvinfo.fr/image/KjNRQzJErXGWAp9rBo7wwQsJgzA/600x400/regions/2020/06/09/5edf5964d4342_maxsportsworld428846-3673067.jpg"
    alt="Homme soulevant trophée de la Ligue des Champions">
</body>
</html>
```

On crée l'application express qui va nous servir à servir la page html

```bash
echo "'use strict';
const express = require('express');
const path = require('path');

const PORT = 80;

const app = express();
app.get('/', (req, res) => {
  console.log(req.ip, ' -> Hello world')
  res.sendFile(path.join(__dirname, '/index.html'));
});
app.listen(PORT);
console.log(`Running on localhost:${PORT}`);" > server.js
```

On crée ensuite le conteneur que l'on va build

```bash
echo 'FROM httpd
EXPOSE 80

COPY index.html htdocs/index.html' > Dockerfile
docker build -t k8s .
```

### Test du conteneur

#### Lancement

```bash
docker run -d -p80:80 --name test_k8s k8s
```

#### Test

On observe la page depuis notre navigateur en tapant `localhost`

![image du site](images/index.png)

#### Arret

```bash
docker stop test_k8s 
```

### Envoie au dockerhub

On se connecte ensuite au dockerhub.

<!-- 77587c65-71e2-41de-a1c4-4ed3b872a4a7-->

```bash
docker login -u jeremylardenois -p <token>
```

Puis on rebuild l'image avec le bon nom et on l'envoie par celui-ci

```bash
docker build -t jeremylardenois/tp-k8s .
docker push jeremylardenois/tp-k8s
```

## Creation du cluster

Pour créer ce cluster, on va utiliser multipass

### Installation de multipass

Ayant déjà snap d'installer, je vais l'utiliser pour installer multipass

```bash
sudo snap install multipass
```

On verifie le bon fonctionnement:

```bash
multipass launch --name foo
multipass ls
```

```console
Name            State             IPv4             Image
foo             Running           10.15.214.206    Ubuntu 20.04 LTS
```

Et on supprime la machine de test

```bash
multipass delete foo
multipass purge
multipass ls
```

> No instances found.

### Creation des machines virtuelles

Pour créer les différentes machines virtuelles, nous allons suivre cette documentation à partir d'ici <https://github.com/zepouet/montpellier.univ/blob/main/multipass-k3s.md#create-nodes> :

```bash
multipass launch --name master-k8s   --cpus 2 --mem 2048M --disk 5G
multipass launch --name worker-1-k8s --cpus 2 --mem 2048M --disk 5G
multipass launch --name worker-2-k8s --cpus 2 --mem 2048M --disk 5G
multipass exec master-k8s -- bash -c "curl -sfL https://get.k3s.io | sh -s - --no-deploy=traefik"
export K3S_IP_MASTER="$(multipass info master-k8s | grep "IPv4" | awk -F' ' '{print $2}'):6443"
export K3S_TOKEN=$(multipass exec master-k8s -- /bin/bash -c "sudo cat /var/lib/rancher/k3s/server/node-token")

multipass exec worker-1-k8s -- /bin/bash -c "curl -sfL https://get.k3s.io | K3S_TOKEN=${K3S_TOKEN} K3S_URL=https://${K3S_IP_MASTER} sh -"
multipass exec worker-2-k8s -- /bin/bash -c "curl -sfL https://get.k3s.io | K3S_TOKEN=${K3S_TOKEN} K3S_URL=https://${K3S_IP_MASTER} sh -"
```

Et l'on vérifie que ça fonctionne :

```bash
multipass exec master-k8s -- sudo kubectl get nodes
```

```console
NAME           STATUS   ROLES                  AGE   VERSION
master-k8s     Ready    control-plane,master   82s   v1.21.2+k3s1
worker-1-k8s   Ready    <none>                 57s   v1.21.2+k3s1
worker-2-k8s   Ready    <none>                 52s   v1.21.2+k3s1
```

## Git et Gitlab

Lien Gitlab: <https://gitlab.com/jeremy_lardenois/tp-k8s>

On crée un projet sur gitlab, et ensuite on tape les commandes suivantes pour envoyer notre "projet" sur le gitlab

```bash
git init
git add index.html Dockerfile
git commit -m "build: initial commit"
git remote add origin git@gitlab.com:jeremy_lardenois/tp-k8s.git
git push --set-upstream origin master
```

## Kubernetes

On commence par se connecter sur le masteravec la commande `multipass exec master-k8s bash` puis on passe root.

### Service Accounts

Pour créer un service account

```bash
kubectl apply -f - <<EOF
apiVersion: v1
kind: ServiceAccount
metadata:
  name: jlardenois
EOF
```

### Namespace

On créer un namespace appelé jlardenois

```bash
kubectl create ns jlardenois
```

On définit ce workspace par défaut

```bash
kubectl config set-context --current --namespace=jlardenois
```

### RBAC

On crée un role

```bash
kubectl create role jlardenois --verb=get,list,watch --resource=secrets,replicaset,deployment,configmap
```

Puis on le lie au service account

```bash
kubectl create rolebinding jlardenois --user=jlardenois --role=jlardenois
```

### Pod

On crée le pod avec l'image du dockerhub

```bash
kubectl run tp-k8s --image=jeremylardenois/tp-k8s  --restart=Never
```

## Gitlab ci

On crée le fichier .gitlab-ci.yml suivant que l'on ajoutera ensuite

```yml
---

```